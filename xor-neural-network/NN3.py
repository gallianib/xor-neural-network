
import numpy as np
import pylab as pl

def nnOutput(alpha, beta, X):
    i = np.ones(np.shape(X)[0]);
    i = np.column_stack((i,X));
    
    j = np.dot(i,alpha);
    j = j.sum(axis=1);
    j = sig(j);
    j2 = np.ones(1);

    j = np.row_stack((j,j2));

    k = np.dot(j,np.transpose(beta));   
    output = k.sum(axis =1);
    output = np.delete(output,(np.shape(output)[0])-1,0);
    return output;    
    
################################################    
## X is the input samples
## Y is the desired outputs
## nHiddens is the number of hidden units
## rhoh is the coefficient for alpha
## rhoo is the coefficient for beta
## mom is the momentum value
## wmax is the range of the weights
## nEpochs is the number of iterations
## alpha is the weights in the hidden layer
## beta is the weight in the output layer 
################################################   
def Train(X, Y, nHiddens, rhoh, rhoo, mom, wmax, nEpochs):
    nSamples = np.shape(X)[0];
    nInputs = np.shape(X)[1];
    nOutputs = np.shape(Y)[1];
    
    lr = rhoo/np.sqrt(nSamples);
    T = rhoh / np.sqrt(nSamples);
    
    alpha = np.random.uniform(-wmax, wmax, (1+nInputs,nHiddens));
    beta = np.random.uniform(-wmax, wmax, (1+nHiddens,nOutputs));
    
    i = np.ones(nSamples);
    i = np.column_stack((i,X));

    for LC in range(0, 5):
        betaCP = beta;
        betaT = np.transpose(beta);
        j = np.dot(i,alpha);
        j = j.sum(axis=1);
        j = sig(j);
        j2 = np.ones(1);

        j = np.row_stack((j,j2));

        k = np.dot(j,betaT);   
        k = k.sum(axis =1);
        k = np.delete(k,nHiddens,0);

        tk = Y-k;

        deltab = np.dot(j,np.transpose(tk));
        


        deltab = (lr*deltab);

        beta = betaCP + deltab;        

        beta = beta.sum(axis=1);


        dbeta = dsig(beta);
        dbeta = np.transpose(np.delete(dbeta,nHiddens,0));
        deltaa = np.dot(tk,beta);
        deltaa = np.multiply(deltaa,dbeta);
        deltaa = np.dot(deltaa,i);
        deltaa = np.dot(deltaa,T);
        deltaaT = np.transpose(deltaa);
        alpha = alpha + (lr*deltaaT);

    return alpha,beta;
   
def sig(x):
    return 1/(1+np.exp(-x));
def dsig(x):
    return np.exp(-x)/np.power((1 + np.exp(-x)),2);

def RunXOR():
    inputs = np.matrix([[0,0], [0,1], [1,0], [1,1]]);
    targets = np.matrix([[0.0], [0.99], [0.99], [0.0]]);
        
    [alpha, beta] = Train(inputs, targets, 4, 1, 0.01, 0.8,  0.1, 1000);
   
    outs = nnOutput(alpha, beta, inputs);
   
    err = (outs - targets);
    RMSE = np.sqrt(np.mean(np.multiply(err,err)));
    
    print(RMSE);
    print(outs);
    
if __name__ == '__main__':
    
    RunXOR();
    